package com.zuitt.capstone.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "course_enrollments")
public class CourseEnrollment {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "course_id")
    @JsonIgnore
    private Course course;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment() {}

    public CourseEnrollment(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public int getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}
